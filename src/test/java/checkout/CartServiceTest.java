package checkout;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import br.com.avaliacao.checkout.Constant;
import br.com.avaliacao.checkout.db.CartDBInMemory;
import br.com.avaliacao.checkout.http.dto.Response;
import br.com.avaliacao.checkout.model.Cart;
import br.com.avaliacao.checkout.model.CartItem;
import br.com.avaliacao.checkout.model.Product;
import br.com.avaliacao.checkout.service.CartService;

public class CartServiceTest {

	@Mock
	private CartService mCartService;
	
	@Mock
	private CartDBInMemory mCartDB;
	
	@Before
	public void init() {
		mCartService = new CartService();
		
		mCartDB = Mockito.mock(CartDBInMemory.class);
	}
	
	@Test
	public void testAddCartWithCartIdZero() {
		CartItem cartItem = mockCartItem();
		
		Response response = mCartService.addCart(0l, cartItem);
		
		Mockito.verify(mCartDB, Mockito.never()).findOne(Mockito.anyLong());
		Mockito.verify(mCartDB, Mockito.never()).save(Mockito.any(Cart.class));
		
		Assert.assertFalse(response.isSuccessCode());
		Assert.assertEquals(response.getMessage(), Constant.CART_ID_MUST_BE_A_POSITIVE_LONG);
	}
	
	@Test
	public void testAddCartWithCartItemNull() {
		Response response = mCartService.addCart(1l, null);
		
		Mockito.verify(mCartDB, Mockito.never()).findOne(Mockito.anyLong());
		Mockito.verify(mCartDB, Mockito.never()).save(Mockito.any(Cart.class));
		
		Assert.assertFalse(response.isSuccessCode());
		Assert.assertEquals(response.getMessage(), Constant.CART_ITEM_MUST_NOT_BE_NULL);
	}
	
	@Test
	public void testAddCartWithProductNull() {
		CartItem cartItem = mockCartItem();
		cartItem.setProduct(null);
		
		Response response = mCartService.addCart(1l, cartItem);
		
		Mockito.verify(mCartDB, Mockito.never()).findOne(Mockito.anyLong());
		Mockito.verify(mCartDB, Mockito.never()).save(Mockito.any(Cart.class));
		
		Assert.assertFalse(response.isSuccessCode());
		Assert.assertEquals(response.getMessage(), Constant.PRODUCT_MUST_NOT_BE_NULL);
	}
	
	@Test
	public void testAddCartWithProductCodeNull() {
		CartItem cartItem = mockCartItem();
		cartItem.getProduct().setCode(null);
		
		Response response = mCartService.addCart(1l, cartItem);
		
		Mockito.verify(mCartDB, Mockito.never()).findOne(Mockito.anyLong());
		Mockito.verify(mCartDB, Mockito.never()).save(Mockito.any(Cart.class));
		
		Assert.assertFalse(response.isSuccessCode());
		Assert.assertEquals(response.getMessage(), Constant.PRODUCT_CODE_MUST_NOT_BE_NULL_OR_EMPTY);
	}
	
	@Test
	public void testAddCartWithProductCodeEmpty() {
		CartItem cartItem = mockCartItem();
		cartItem.getProduct().setCode("");
		
		Response response = mCartService.addCart(1l, cartItem);
		
		Mockito.verify(mCartDB, Mockito.never()).findOne(Mockito.anyLong());
		Mockito.verify(mCartDB, Mockito.never()).save(Mockito.any(Cart.class));
		
		Assert.assertFalse(response.isSuccessCode());
		Assert.assertEquals(response.getMessage(), Constant.PRODUCT_CODE_MUST_NOT_BE_NULL_OR_EMPTY);
	}
	
	@Test
	public void testAddCartWithProductPriceZero() {
		CartItem cartItem = mockCartItem();
		cartItem.getProduct().setPrice(0d);
		
		Response response = mCartService.addCart(1l, cartItem);
		
		Mockito.verify(mCartDB, Mockito.never()).findOne(Mockito.anyLong());
		Mockito.verify(mCartDB, Mockito.never()).save(Mockito.any(Cart.class));
		
		Assert.assertFalse(response.isSuccessCode());
		Assert.assertEquals(response.getMessage(), Constant.PRODUCT_PRICE_MUST_BE_POSITIVE);
	}

	@Test
	public void testAddCartWithProductQuantityZero() {
		CartItem cartItem = mockCartItem();
		cartItem.setQuantity(0);
		
		Response response = mCartService.addCart(1l, cartItem);
		
		Mockito.verify(mCartDB, Mockito.never()).findOne(Mockito.anyLong());
		Mockito.verify(mCartDB, Mockito.never()).save(Mockito.any(Cart.class));
		
		Assert.assertFalse(response.isSuccessCode());
		Assert.assertEquals(response.getMessage(), Constant.QUANTITY_OF_THE_PRODUCT_MUST_BE_POSITIVE);
	}

	@Test
	public void testAddCartWithSuccess() {
		long cartId = 1l;
		CartItem cartItem = mockCartItem();
		
		Response response = mCartService.addCart(cartId, cartItem);
		
		Assert.assertTrue(response.isSuccessCode());
		Assert.assertEquals(response.getMessage(), Constant.SUCCESS);
	}
	
	private CartItem mockCartItem() {
		Product product = new Product();
		product.setCode("ABC123");
		product.setName("Camisa Polo");
		product.setBrand("Cavalera");
		product.setPrice(129.9);
		
		CartItem cartItem = new CartItem();
		cartItem.setProduct(product);
		cartItem.setQuantity(2);
		
		return cartItem;
	}
}
