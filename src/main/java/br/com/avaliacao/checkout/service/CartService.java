package br.com.avaliacao.checkout.service;

import org.springframework.stereotype.Service;

import br.com.avaliacao.checkout.Constant;
import br.com.avaliacao.checkout.db.CartDBInMemory;
import br.com.avaliacao.checkout.http.dto.Response;
import br.com.avaliacao.checkout.model.Cart;
import br.com.avaliacao.checkout.model.CartItem;

@Service
public class CartService {

	private CartDBInMemory cartDB = CartDBInMemory.getInstance();

	public Response addCart(Long cartId, CartItem cartItem) {
		Response response = new Response();

		if(cartId <= 0) {
			response.setMessage(Constant.CART_ID_MUST_BE_A_POSITIVE_LONG);			
		}
		else if(cartItem == null) {
			response.setMessage(Constant.CART_ITEM_MUST_NOT_BE_NULL);
		}
		else if(cartItem.getProduct() == null) {
			response.setMessage(Constant.PRODUCT_MUST_NOT_BE_NULL);
		}
		else if(cartItem.getProduct().getCode() == null || cartItem.getProduct().getCode().isEmpty()) {
			response.setMessage(Constant.PRODUCT_CODE_MUST_NOT_BE_NULL_OR_EMPTY);
		}
		else if(cartItem.getProduct().getPrice() <= 0) {
			response.setMessage(Constant.PRODUCT_PRICE_MUST_BE_POSITIVE);
		}
		else if(cartItem.getQuantity() <= 0) {
			response.setMessage(Constant.QUANTITY_OF_THE_PRODUCT_MUST_BE_POSITIVE);
		}

		if(response.getMessage() == null) {
			String codeKey = cartItem.getProduct().getCode();

			Cart cart = cartDB.findOne(cartId);

			if(cart == null) {
				cart = new Cart();
				cart.setCartId(cartId);
			}

			if(cart.getItems().containsKey(codeKey)){
				cartItem.addQuantity(cart.getItems().get(codeKey).getQuantity());
			}

			cart.getItems().put(codeKey, cartItem);

			cartDB.save(cart);
			
			response.setMessage(Constant.SUCCESS);
			response.setSuccessCode(true);
		}
		else {
			response.setSuccessCode(false);
		}

		return response;
	}
}