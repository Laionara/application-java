package br.com.avaliacao.checkout.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Product {

    private String code;
    private String name;
    private String brand;
    private Double price;
}