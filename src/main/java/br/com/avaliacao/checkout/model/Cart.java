package br.com.avaliacao.checkout.model;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Cart {

    public enum CartStatus {OPENED, ACCOMPLISHED, ABANDONED}
   
    private Long cartId;

    private Map<String, CartItem> items;

    private CartStatus status = CartStatus.OPENED;

    public Map<String, CartItem> getItems() {
        if (items == null) {
            items = new HashMap<>();
        }
        return items;
    }

    public boolean isOpened() {
        return CartStatus.OPENED.equals(this.status);
    }

    public boolean isAccomplished() {
        return CartStatus.ACCOMPLISHED.equals(this.status);
    }

    public void accomplished() {
        this.status = CartStatus.ACCOMPLISHED;
    }

    public Double getPrice() {
        Double total = 0d;
        
        for (CartItem cartItem : items.values()) {
            total += cartItem.getPrice();
        }
        return total;
    }
}
