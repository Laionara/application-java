package br.com.avaliacao.checkout.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CartItem {

    private Product product;

    private Integer quantity;

    public Double getPrice() {
        return quantity * product.getPrice();
    }

    public void addQuantity(final Integer quantity) {
        this.quantity += quantity;
    }
}