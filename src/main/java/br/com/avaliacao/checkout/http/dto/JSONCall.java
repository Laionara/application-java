package br.com.avaliacao.checkout.http.dto;

import br.com.avaliacao.checkout.model.CartItem;
import lombok.Getter;
import lombok.Setter;

public class JSONCall {

	@Getter @Setter
	public Long cartId;
	
	@Getter @Setter
	public CartItem cartItem;
}
