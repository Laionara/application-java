package br.com.avaliacao.checkout.http;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.avaliacao.checkout.http.dto.JSONCall;
import br.com.avaliacao.checkout.http.dto.Response;
import br.com.avaliacao.checkout.model.CartItem;
import br.com.avaliacao.checkout.service.CartService;

@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;
    
    /*
     * Example of Body: 
		{
		  "cartId":1,
		  "cartItem": {
		      "product": {
		          "code": "ABC123",
		          "name": "Sapato Social",
		          "brand": "Nike",
		          "price": 128.7
		      },
		      "quantity": 3
		  }
		}
     */
    @RequestMapping(value = "/adicionar", method = RequestMethod.POST)
    public @ResponseBody Response addToCart(@RequestBody JSONCall jSONCall) {
    	Long cartId = jSONCall.getCartId();
    	CartItem cartItem = jSONCall.getCartItem();
        return this.cartService.addCart(cartId, cartItem);
    }
}