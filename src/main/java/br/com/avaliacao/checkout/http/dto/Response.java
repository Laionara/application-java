package br.com.avaliacao.checkout.http.dto;

import lombok.Getter;
import lombok.Setter;

public class Response {
	@Getter @Setter
	private boolean successCode;

	@Getter @Setter
	private String message;
}
