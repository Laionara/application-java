package br.com.avaliacao.checkout;

public class Constant {

	//Success Message Value
	public static final String SUCCESS = "operation with success";
	
	//Add to Cart Message Values
	public static final String QUANTITY_OF_THE_PRODUCT_MUST_BE_POSITIVE = "quantity of the product must be positive";
	public static final String PRODUCT_PRICE_MUST_BE_POSITIVE = "product price must be positive";
	public static final String PRODUCT_CODE_MUST_NOT_BE_NULL_OR_EMPTY = "product code must not be null or empty";
	public static final String PRODUCT_MUST_NOT_BE_NULL = "product must not be null";
	public static final String CART_ITEM_MUST_NOT_BE_NULL = "cartItem must not be null";
	public static final String CART_ID_MUST_BE_A_POSITIVE_LONG = "cartId must be a positive long";
}
